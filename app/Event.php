<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
     protected $fillable = [
        'name', 'duration'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}