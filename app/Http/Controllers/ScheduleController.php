<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use Validator,Redirect,Response;
use App\Schedule;

class ScheduleController extends Controller
{
    public function index(Request $request, $userId)
    {
        $events = Event::where('user_id', $userId)->get();
        $user = User::select('name', 'id')->where('id', $userId)->first();
        return view('landing', [
            'events' => $events,
            'user' => $user
        ]);
    }


    public function schedule(Request $request, $userId, $eventId)
    {
    	
        $event = Event::select('name', 'duration','id')->where('id', $eventId)->first();
        $user = User::select('name', 'id')->where('id', $userId)->first();
        $schedules = Schedule::where('user_id', $userId)->orderBy('start_time', 'ASC')->get();
        return view('schedule', [
            'event' => $event,
            'user' => $user,
            'schedules' => $schedules
        ]);
    }

    public function createSchedule(Request $request){
    	request()->validate([
        'user_id' => 'required|numeric',
        'user_name' => 'required|max:255',
        'event' => 'required|max:255',
        'event_id' =>  'required|numeric',
        'duration' => 'required|numeric',
        'time' => 'required',
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'email' => 'required|email|max:255',
        ]);

    	$startTime = strtotime($request['time']);
    	$duration = $request['duration'];
    	$endTime = strtotime('+'.$duration.' minutes',$startTime);
    	$endTimeFormatted = date('Y-m-d H:i:s',$endTime);

        Schedule::create([
        'user_id' => $request['user_id'],
        'event_id' => $request['event_id'],
        'start_time' => $request['time'],
        'end_time' => $endTimeFormatted,
        'firstname' => $request['first_name'],
        'lastname' => $request['last_name'],
        'email' => $request['email'],
        ]);

        return Redirect::to("success")
        ->withSuccess('Event Scheduled Successfully!')
        ->withUser($request['user_name'])
        ->withId($request['user_id'])
        ->withEvent($request['event'])
        ->withTime($request['time']);
    }
}
