<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Validator,Redirect,Response;
use App\Schedule;
use App\User;

class EventController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $events = Event::where('user_id', $request->user()->id)->get();
        $user = User::select('name')->where('id',$request->user()->id)->first();

        return view('events', [
            'events' => $events,
            'user' => $user,
            'link' => '/appointment/'.$request->user()->id
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $user = User::select('name')->where('id',$request->user()->id)->first();
        return view('createvent', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->has('event_duration')){
           request()->validate([
            'event_name' => 'required|max:255',
            'event_duration' => 'required|numeric|min:5'
            ]);
           $data = $request->all();
            $request->user()->event()->create([
                'name' => $data['event_name'],
                'duration' => $data['event_duration']
            ]);
        }else{
            request()->validate([
            'event_name' => 'required|max:255',
            'event_duration_custom' => 'required|numeric|min:5'
            ]);
            $data = $request->all();
            $request->user()->event()->create([
                'name' => $data['event_name'],
                'duration' => $data['event_duration_custom']
            ]);
        }

        return Redirect::to("event")->withSuccess('Event Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        
        $event = Event::where('user_id', $request->user()->id)->where('id', $id)->first();
        $user = User::select('name')->where('id',$request->user()->id)->first();
        return view('createvent', [ 'event' => $event , 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('event_duration')){
           request()->validate([
            'event_name' => 'required|max:255',
            'event_duration' => 'required|numeric|min:5'
            ]);
           $data = $request->all();
            $request->user()->event()->where('id',$id)->update([
                'name' => $data['event_name'],
                'duration' => $data['event_duration']
            ]);
        }else{
            request()->validate([
            'event_name' => 'required|max:255',
            'event_duration_custom' => 'required|numeric|min:5'
            ]);
            $data = $request->all();
            $request->user()->event()->where('id',$id)->update([
                'name' => $data['event_name'],
                'duration' => $data['event_duration_custom']
            ]);
        }
        return Redirect::to("event")->withSuccess('Event Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Event $event)
    {
        $this->authorize('destroy', $event);
        $event->delete();

        return redirect('/event')->withSuccess('Event Deleted Successfully!');
    }

    // public function schedule(){
    //     return view('schedules');
    // }

    public function mySchedules(Request $request){
        $schedules = Schedule::where('schedules.user_id', $request->user()->id)
        ->join('events', 'schedules.event_id', '=', 'events.id')
        ->select('schedules.firstname', 'schedules.lastname', 'schedules.start_time', 'schedules.end_time','events.name')->get();

        return datatables()->of($schedules)
            ->make(true);
    }
}
