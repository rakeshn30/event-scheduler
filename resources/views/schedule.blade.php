@extends('app')

@section('title')
    <title> Schedule Appointment</title>
@endsection

@section('main')
      <div class="login d-flex align-items-center py-5">
        <div class="container-fluid">
          <div class="row">
            @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
  </div>

  <div class="card">
    <div class="card-header">{{ $user->name }}
       <button class="float-right btn btn-primary" onclick="window.history.go(-1)">Back</button>
    </div>
    <div class="card-body">Event {{ $event->name }}</div>
    <div class="card-footer">Duration {{ $event->duration }}</div>
  </div>
  <hr>
  <div class="container">
          
          <form method = "post" action="/createSchedule">
            @csrf
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="user_name" value="{{ $user->name }}">
            <input type="hidden" name="duration" value="{{ $event->duration }}">
            <input type="hidden" name="event" value="{{ $event->name }}">
            <input type="hidden" name="event_id" value="{{ $event->id }}">
          <div class="form-group">    
              <label for="time">Select Time</label>
              <input type="text" class="form-control" name="time" id="datetimepicker" value="" />
          </div>  
          <div class="form-group">    
              <label for="first_name">First Name</label>
              <input type="text" class="form-control" name="first_name" value="" />
          </div>  
          <div class="form-group">    
              <label for="last_name">Last Name</label>
              <input type="text" class="form-control" name="last_name" />
          </div>  
          <div class="form-group">    
              <label for="email">Email</label>
              <input type="text" class="form-control" name="email" />
          </div>  
          <button type="submit" class="btn btn-primary-outline">Schedule Event</button>
      </form>
        </div>
      </div>
    </div>
<?php 
$minutesArray = array();
foreach ($schedules as $key => $schedule) {
  $startTime = $schedule->start_time;
  $endTime = $schedule->end_time;

  $StartTime = strtotime($startTime);
  $EndTime = strtotime($endTime);
  $AddMins  = 15 * 60;
  while ($StartTime <= $EndTime) //Run loop
    {
        $interval = date ("Y-m-d H:i:s", $StartTime);
        array_push($minutesArray, $interval);
        $StartTime += $AddMins; //Endtime check
    }
}

?>
<script type="text/javascript">
  var minutesArray = '<?php echo json_encode($minutesArray); ?>';
</script>

@endsection