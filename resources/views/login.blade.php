@extends('app')

@section('title')
    <title> Login </title>
@endsection

@section('main')
            	@if (Session::has('success'))
                <div class="alert alert-success">
                   <p>{{Session::get('success') }}</p>
                </div>
              @endif
              @if (Session::has('danger'))
                <div class="alert alert-danger">
                   <p>{{Session::get('danger') }}</p>
                </div>
              @endif
              <h3 class="login-heading mb-4">Welcome back!</h3>
               <form action="{{url('post-login')}}" method="POST" id="logForm">

                 {{ csrf_field() }}

                <div class="form-label-group">
                  <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" >
                  <label for="inputEmail">Email address</label>

                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif    
                </div> 

                <div class="form-label-group">
                  <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password">
                  <label for="inputPassword">Password</label>
                  
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif  
                </div>

                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Sign In</button>
                <div class="text-center">If you have an account?
                  <a class="small" href="{{url('registration')}}">Sign Up</a></div>
              </form>
@endsection