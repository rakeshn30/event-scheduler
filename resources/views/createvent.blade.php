@extends('base')

@section('title')
  @if (isset($event->name)) 
  <title> Update Event </title> 
  @else 
  <title> Add Event </title>
  @endif 
    
@endsection

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
  <div class="card">
    <div class="card-header">@if (isset($event->name)) {{ 'Update' }} @else {{ 'Add' }} @endif Event
            <button class="float-right btn btn-primary" onclick="window.history.go(-1)">Back</button>
        </div>
  </div>
    
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
       @if(!isset($event->id))
            <form method = "post" action="/event">
            @else
             <form method = "post" action="/event/{{$event->id}}">
            @endif
          @csrf
          @isset($event->id)
           {{ method_field('PATCH')}}
          @endisset
          <div class="form-group">    
              <label for="first_name">Event Name</label>
              <input type="text" class="form-control" name="event_name" value="@if (isset($event->name)) {{$event->name}} @endif" />
          </div>  
          <div class="form-group">    
              <label for="first_name">Event Duration</label>
              
              <label class="container">15min
                <input type="radio"  @if (isset($event->duration) && $event->duration == 15 ) {{ 'checked' }} @endif name="event_duration" value="15">
                <span class="checkmark"></span>
              </label>
              <label class="container">30min
                <input type="radio"  @if (isset($event->duration) && $event->duration == 30 ) {{ 'checked' }} @endif name="event_duration" value="30">
                <span class="checkmark"></span>
              </label>
              <label class="container">45min
                <input type="radio" @if (isset($event->duration) && $event->duration == 45 ) {{ 'checked' }} @endif name="event_duration" value="45">
                <span class="checkmark"></span>
              </label>
              <label class="container">60min
                <input type="radio" @if (isset($event->duration) && $event->duration == 60 ) {{ 'checked' }} @endif name="event_duration" value="60">
                <span class="checkmark"></span>
              </label>
              <input type="text" style="width:20%"; value="@if (isset($event->duration) && $event->duration != 15 && $event->duration != 30 && $event->duration != 45 && $event->duration != 60 ) {{ $event->duration }} @endif" class="form-control" id="event_duration" name="event_duration_custom"/>
          </div>                             
          <button type="submit" class="btn btn-primary-outline">@if (isset($event->name)) {{ 'Update' }} @else {{ 'Add' }} @endif Event</button>
      </form>
  </div>
</div>
</div>
@endsection