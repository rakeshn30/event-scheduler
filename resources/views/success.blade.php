@extends('app')

@section('title')
    <title> Success </title>
@endsection

@section('main')
              @if (Session::has('success'))
                <div class="alert alert-success">
                   <p>{{Session::get('success') }}</p>
                </div>
              @endif
              @if (Session::has('user'))
                <div class="alert alert-success">
                   <p>Your schedule with {{Session::get('user') }} is confirmed!</p>
                </div>
              @endif
              @if (Session::has('event'))
                <div class="alert alert-success">
                   <p>Event {{Session::get('event') }}</p>
                </div>
              @endif
              @if (Session::has('time'))
                <div class="alert alert-success">
                   <p>Time {{Session::get('time') }}</p>
                </div>
              @endif
               @if (Session::has('id'))
               <a href="/appointment/{{Session::get('id')}}">Schedule Another Event</a>
               @else
                <a href="/">Home</a>
               @endif
              
@endsection