<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  @yield('title')
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/bootstrap.datetimepicker.js') }}"></script>
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary rounded" style="margin-bottom: 10px;">
 
  <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <a class="nav-link">{{ $user->name }}</a>
    </li>
    <li class="nav-item">
      <a href="{{url('logout')}}" class="nav-link">Logout</a>
    </li>
  </ul>
</nav>
  <div class="container">

  	<!-- <div class="card-body"> -->
    
    @yield('main')
    <!-- </div> -->
  </div>
  <script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>
