@extends('app')

@section('title')
    <title> Schedule Appointment</title>
@endsection

@section('main')
            <div class="card">
                <div  class="card-body">
                    Welcome to the scheduling page of {{ $user->name }}
                </div>
            </div>
        <div class="container">
           
          <div class="row">
            @if (count($events) > 0)
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Events</th>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($events as $event)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <a href="/appointment/{{$user->id}}/event/{{$event->id}}">{{ $event->name }}</a>
                                </td>
                                <td class="table-text">
                                    <div>{{ $event->duration }} mins</div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
    @else
      <h3>No events found!</h3>
    @endif
          </div>
        </div>
@endsection