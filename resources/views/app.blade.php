<!DOCTYPE html>
<html>
<head>
@yield('title')

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.datetimepicker.css')}}">
<script type="text/javascript" src="{{ asset('js/bootstrap.datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

</head>
<body>
<div class="container-fluid">
  <div class="row no-gutter">
    <!-- <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div> -->
    <div class="col-md-12">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
            	@yield('main')
</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>