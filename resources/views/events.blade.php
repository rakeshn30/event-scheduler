@extends('base')

@section('title')
    <title> Dashboard </title>
@endsection

@section('main')
	@if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if (Session::has('success'))
    <div class="alert alert-success">
       <p>{{Session::get('success') }}</p>
    </div>
  @endif

    <div class="card">
        <div class="card-header">My Link
            <a class="float-right" href="/event/create">Add Event</a>
        </div>
        <div class="card-body">
            <a href="{{ $link }}">{{ $link }}</a>
        </div>
    </div>
    <div class="bs-example" style="margin-top: 15px;">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="#events" class="nav-link active" data-toggle="tab">Events</a>
        </li>
        <li class="nav-item">
            <a href="#schedule" class="nav-link" data-toggle="tab">Schedule</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="events">
            @if (count($events) > 0)
        <div class="panel panel-default">


            <div class="panel-body">
                <table class="table table-striped task-table">

                    <!-- Table Headings -->
                    <thead>
                        <th>Events</th>
                        <th>&nbsp;</th>
                    </thead>

                    <!-- Table Body -->
                    <tbody>
                        @foreach ($events as $event)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-text">
                                    <div>{{ $event->name }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $event->duration }} mins</div>
                                </td>

                                <td>
                                    <a href="/event/{{ $event->id }}/edit">Edit Event</a>
                                </td>

                                <td>
                                    <form action="/event/{{ $event->id }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <button>Delete Event</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        <h3>No events found!</h3>
    @endif
        </div>
        <div class="tab-pane fade" id="schedule">
            <div class="container">
               <h4>My Schedules</h4>
            <table class="table table-bordered" id="laravel_datatable" style="width: 100%">
               <thead>
                  <tr>
                     <th>Start Time</th>
                     <th>End Time</th>
                     <th>First Name</th>
                     <th>Last Name</th>
                     <th>Event Name</th>
                  </tr>
               </thead>
            </table>
         </div>
        </div>
    </div>
</div>
    

	
@endsection