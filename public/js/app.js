jQuery(document).ready(function(){

	jQuery('#event_duration').focus(function(){
		jQuery('input[name="event_duration"]').prop('checked', false);
	})

	jQuery('input[name="event_duration"][type="radio"]').change(function(){
		jQuery('#event_duration').val('');
	});

	jQuery('#laravel_datatable').DataTable({
       processing: true,
       serverSide: true,
       ajax: "/myschedulelist",
       columns: [
                { data: 'start_time', name: 'start_time' },
                { data: 'end_time', name: 'end_time' },
                { data: 'firstname', name: 'firstname' },
                { data: 'lastname', name: 'lastname' },
                { data: 'name', name: 'name' }
             ]
    });

	var startdate = new Date();
	var disableHours = [];
	var disableDates = [];

	if(typeof minutesArray != 'undefined' && minutesArray != ''){
		var disableMinutes = JSON.parse(minutesArray);

		for (var i in disableMinutes){
			// console.log(typeof disableMinutes[i]);
			if(disableMinutes[i].indexOf("00:00") > -1){
				var end = disableMinutes[i].replace('00:00', '45:00');
				if(disableMinutes.indexOf(end) > -1){
					disableHours.push(disableMinutes[i]);
				}
			}
		}
	}else{
		var disableMinutes = [];
	}

	for (var i in disableHours){
		if(disableHours[i].indexOf("10:00:00") > -1){
			var dateArr = disableHours[i].split(' ');
			var date = dateArr[0];
			count = 0;
		}
		if(typeof count != 'undefined'){
			count++;
		}
		
		if(typeof count != 'undefined' && count == 9){
			disableDates.push(date);
		}
	}
	


	jQuery('#datetimepicker').datetimepicker(
		{  
			startDate: startdate,
			daysOfWeekDisabled: [0,6],
			hoursDisabled: [0,1,2,3,4,5,6,7,8,9,19,20,21,22,23],
			minuteStep: 15,
			onRenderHour:function(date){
				var newdate = formatDate(date)+" "+date.getUTCHours();
				mins = '00';
				seconds = '00';

				var finalDate = newdate +":"+mins+":"+seconds;
			  	if(disableHours.indexOf(finalDate)>-1)
			    {
			        return ['disabled'];
			    }
			},
			onRenderMinute:function(date){
				var newdate = formatDate(date)+" "+date.getUTCHours();
				var mins = date.getUTCMinutes();
				var seconds = date.getUTCSeconds();
				if(mins == 0){
					mins = '00';
				}

				if(seconds == 0){
					seconds = '00';
				}

				var finalDate = newdate +":"+mins+":"+seconds;
			  	if(disableMinutes.indexOf(finalDate)>-1)
			    {
			        return ['disabled'];
			    }
			},
			onRenderDay: function(date){
				var newdate = formatDate(date);
				if(disableDates.indexOf(newdate)>-1)
			    {
			        return ['disabled'];
			    }
			}
		});
});

function formatDate(datestr)
{
    var date = new Date(datestr);
    var day = date.getDate(); day = day>9?day:"0"+day;
    var month = date.getMonth()+1; month = month>9?month:"0"+month;
    return date.getFullYear()+"-"+month+"-"+day;
}
