<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
        return Redirect::to("event");
    }
    return Redirect::to("login");
});


Route::get('login', [ 'as' => 'login', 'uses' => 'HomeController@index']);
Route::post('post-login', 'HomeController@postLogin'); 
Route::get('registration', 'HomeController@registration');
Route::post('post-registration', 'HomeController@postRegistration'); 
Route::get('dashboard', 'HomeController@dashboard'); 
Route::get('logout', 'HomeController@logout');


Route::resource('event', 'EventController');
Route::get('myschedules', 'EventController@schedule');
Route::get('myschedulelist', 'EventController@mySchedules');
Route::get('appointment/{userid}', 'ScheduleController@index');
Route::get('appointment/{userid}/event/{eventId}', 'ScheduleController@schedule');

Route::post('createSchedule', 'ScheduleController@createSchedule');
Route::get('success', function(){
	return view('success');
});